import gulp from 'gulp'
import * as sass from 'sass'
import gulpsass from 'gulp-sass'
import concatCss from 'gulp-concat-css'
import browserSync from 'browser-sync'


const sassPlugin = gulpsass(sass)
const browser = browserSync.create()

export function html(){
    return gulp.src('src/index.html')
    .pipe(gulp.dest('build'))
}

export function img(){
    return gulp.src('./src/img/**/*.*')
    .pipe(gulp.dest('build/img'))
}

export function css(){
    return gulp.src('src/scss/**/*.scss')
    .pipe(sassPlugin())
    .pipe(concatCss('style.css'))
    .pipe(gulp.dest('build/css'))
}

function reload(cb){
    browser.reload()
    cb()
}

export function watch(){

    browser.init({
        server: {
            baseDir: 'build'
        }
    })

    return gulp.watch('src/**/*.*', gulp.series(gulp.parallel(html, css), reload))
}

export default gulp.parallel(html, css)

